"""Helper class to build a template configuration file"""

from __future__ import annotations

from typing import Optional

from configblock.configblock import ConfigBlock, OptionData
from configblock.dictwriter import DictWriter
from configblock.path import Path


class TemplateBuilder(DictWriter):
    """Create a template configuration dictionary from a configblock

    Creates a dictionary with all the options set to their default values for a
    user to edit.
    """

    def __init__(
        self,
        extra: dict[Path | str, Optional[str]],
        only_active: bool = False,
        default="REQUIRED",
        convert_values: bool = True,
    ) -> None:
        """Create the writer

        Parameters
        ----------
        extra : Dict[Path, Dict[str, str]]
            Keys that should be set in the output dictionary. Used to fill in
            extra subconfigurations
        only_active : bool
            Only iterate through active subconfigurations
        default
            Where a required option is not set, enter this value in the dictionary
        convert_values : bool
            If True, use the convert_value function to convert values before
            they're added to the dictionary
        """
        super().__init__(
            only_active=only_active, default=default, convert_values=convert_values
        )
        self._extra: dict[Path, dict[str, Optional[str]]] = {}
        # Convert all paths to absolute paths
        for k, v in extra.items():
            path = "/" / Path(k)
            # Ensure that all the subconfigurations along the path are set
            for idx in range(1, len(path)):
                self._extra.setdefault(path[:idx], {})
            leading = path[:-1]
            final = str(path[-1])
            self._extra[leading][final] = v

    def begin_subcfg(self, key: str, subcfg: ConfigBlock) -> bool:
        if subcfg._set_from_single_value:
            self.current_dict[key] = self._default if subcfg._always_active else None
            return False
        else:
            return super().begin_subcfg(key, subcfg)

    def begin_block(self) -> bool:
        full_path = self.current_block.full_path
        try:
            extra = self._extra[full_path]
        except KeyError:
            pass
        else:
            for key, type in extra.items():
                if type is not None:
                    self.current_dict[key] = {"Type": type}
                    self.current_block[key] = self.current_dict[key]
                else:
                    self.current_block[key] = {}

        return True

    def visit_option(self, key: str, option: OptionData) -> None:
        if self.current_block.is_set_from_key(key):
            return
        if option.default is None:
            if option.required:
                self.current_dict[key] = self._default
            else:
                self.current_dict[key] = "OPTIONAL"
        elif not callable(option.default):
            self.current_dict[key] = (
                self.convert_value(option.default)
                if self._convert_values
                else option.default
            )
