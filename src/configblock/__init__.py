"""Hierarchical, self documenting configuration structure"""

__version__ = "0.0.1"

import enum

from configblock.utils import convert_out

convert_out.register(enum.Enum, lambda v: v.name)
