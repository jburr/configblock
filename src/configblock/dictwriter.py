"""Class to produce a dictionary from a hierarchy of config blocks"""

from __future__ import annotations

from typing import Any, Optional
from configblock.configblock import ConfigBlock, OptionData
from configblock.visitorbase import Section, VisitorBase
from configblock.utils import convert_out


class DictWriter(VisitorBase):
    """Visitor that creates a dictionary representation of the hierarchy"""

    #: Tag to show that no value has been set
    _no_default = object()

    def __init__(
        self, only_active: bool = True, default=_no_default, convert_values: bool = True
    ) -> None:
        """Create the writer

        Parameters
        ----------
        only_active : bool
            Only write from active subconfigurations
        default : Any
            The value to use for any required options that have not been set.
            If left as DictWriter._no_default encountering a required option
            that hasn't been set will raise a KeyError
        convert_values : bool
            If True, use the convert_value function to convert values before
            they're added to the dictionary
        """
        super().__init__(
            only_active=only_active, section_order=(Section.Options, Section.Subconfigs)
        )
        self._dict: Optional[dict[str, Any]] = None
        self._dct_stack: list[dict[str, Any]] = []
        self._default = default
        self._convert_values = convert_values

    @property
    def current_dict(self) -> dict[str, Any]:
        return self._dct_stack[-1]

    def convert_value(self, value: Any) -> Any:
        """Convert a value for output

        New conversion functions can be registered using
        utils.convert_out.register but note that this will add the
        converter to *all* DictWriters. Otherwise a subclass can just override
        this method as normal.
        """
        return convert_out(value)

    def visit(self, root: ConfigBlock) -> dict[str, Any]:
        """Return a dictionary representation of the provided hierarchy"""
        self._dict = {}
        self._dct_stack.append(self._dict)
        super().visit(root)
        dct = self._dict
        self._dict = None
        assert dct == self.current_dict
        self._dct_stack.pop()
        return dct

    def begin_globals(self) -> bool:
        self._dct_stack.append(self.current_dict.setdefault("Globals", {}))
        return True

    def end_globals(self) -> None:
        assert len(self._dct_stack) > 1
        self._dct_stack.pop()

    def begin_subcfg(self, key: str, subcfg: ConfigBlock) -> bool:
        new_dct = self.current_dict.setdefault(key, {})
        if self.current_block.is_extra_subconfig(key):
            if (
                subcfg_type := self.current_block.extra_subconfig_type(key)
            ) is not None:
                new_dct["Type"] = subcfg_type
        self._dct_stack.append(new_dct)
        return True

    def end_subcfg(self) -> None:
        assert len(self._dct_stack) > 1
        self._dct_stack.pop()

    def visit_option(self, key: str, option: OptionData) -> None:
        assert self.current_block is not None
        if self._default is self._no_default:
            # Let this raise the KeyError
            value = self.current_block[key]
        else:
            value = self.current_block.get(key, self._default)
        if self._convert_values:
            value = self.convert_value(value)
        self.current_dict[key] = value
