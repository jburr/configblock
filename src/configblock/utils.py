"""Helper functions for working with ConfigBlocks"""

from __future__ import annotations

import copy
import functools
import inspect
from typing import Any, Callable, Optional, Tuple, Type
from configblock.configblock import ConfigBlock
import logging

log = logging.getLogger(__name__)

@functools.singledispatch
def convert_out(value: Any) -> Any:
    return value

def mk_optional(func: Callable[[Any], Any]):
    """Return a modified function that handles optional values correctly

    When the first argument to the wrapped function is 'None' then the modified function
    returns None.
    """

    @functools.wraps(func)
    def f(arg, *args, **kwargs):
        if arg is None:
            return None
        else:
            return func(arg, *args, **kwargs)

    if f.__doc__:
        f.__doc__ += "\nIf the first argument is None, return None"
    return f


def wrap_deferred_func(
    func: Callable,
    doc: Optional[str] = None,
    rename_: Optional[dict[str, str]] = None,
    setdefault_: Optional[dict[str, Any]] = None,
    make_optional: bool = False,
):
    """Wrap another function so it can be used as a deferred value for a config block

    Deferred values take the config block as an argument which acts similarly to a
    python dictionary. However most 'normal' functions will just take a list of named
    arguments. This  function provides a way to wrap a 'normal' function so that it
    reads the relevant arguments from the configuration block.

    Parameters
    ----------
    func : Callable
        The function to wrap
    doc : str, optional
        A docstring for the function (used by help printers), otherwise use the wrapped
        function's docstring.
    rename : Dict[str, str], optional
        Mapping from the parameter name in the wrapped function to the name of the
        corresponding option. Any kwargs to be used must be given here
    setdefault : Dict[str, Any], optional
        Set any extra parameters for the function from here
    make_optional : bool
        If True, apply mk_optional to the function

    Raises
    ------
    ValueError
        The function contains a varargs argument: this is not supported
    """
    rename = {} if rename_ is None else rename_
    setdefault = {} if setdefault_ is None else setdefault_

    sig = inspect.signature(func)
    required: list[str] = []
    optional: list[str] = []
    for name, param in sig.parameters.items():
        if (
            param.kind == inspect.Parameter.VAR_POSITIONAL
            or param.kind == inspect.Parameter.POSITIONAL_ONLY
        ):
            raise ValueError("Functions taking positional arguments are not supported")
        elif (
            param.kind == inspect.Parameter.POSITIONAL_OR_KEYWORD
            or param.kind == inspect.Parameter.KEYWORD_ONLY
        ):
            if param.default == inspect.Parameter.empty:
                required.append(name)
            else:
                optional.append(name)

    def f(cfg: ConfigBlock):
        params = copy.copy(setdefault)
        try:
            params.update({k: cfg[rename.get(k, k)] for k in required})
        except KeyError as e:
            log.error(f"Config block does not contain requested key '{e.args[0]}'")
            raise e
        for k in optional:
            try:
                params[k] = cfg[rename.get(k, k)]
            except KeyError:
                pass
        return f.__wrapped__(**params)  # type: ignore[attr-defined]

    f.__wrapped__ = mk_optional(func) if make_optional else func  # type: ignore[attr-defined]
    # Set the docstring
    f.__doc__ = inspect.getdoc(func) if doc is None else doc
    return f


def find_blocks(
    cfg: ConfigBlock,
    condition: Callable[[ConfigBlock], bool],
    max_steps: int = -1,
    only_active: bool = True,
) -> Tuple[ConfigBlock, ...]:
    """Find the closest config blocks in the overall hierarchy that satisfy a condition

    First the children of the provided block are searched. Then if none are found the
    process is repeated on the parent block. This is continued until the root of the
    hierarchy is reached or the max_steps up have been performed.

    Parameters
    ----------
    cfg : ConfigBlock
        The starting block
    condition : Callable[[ConfigBlock], bool]
        The condition to require on the returned configuration blocks
    max_steps : int, optional
        The number of steps up to allow. A value of 0 means that only the children of
        cfg are searched. A negative value guarantees searching the entire hierarchy.
        Defaults to -1
    only_active : bool, optional
        Only consider active blocks.

    Returns
    -------
    Tuple[ConfigBlock, ...]
        The first encountered non-empty tuple of matching blocks. Empty if no matching
        blocks are found.
    """
    subcfgs = tuple(
        c
        for _, c in cfg.iter_subconfigs(nested=True, only_active=only_active)
        if condition(c)
    )
    if subcfgs:
        return subcfgs
    elif max_steps != 0 and cfg.parent is not None:
        return find_blocks(cfg.parent, condition, max_steps, only_active)
    else:
        return ()


def find_option_on_matching_block(
    subcfg_condition: Callable[[ConfigBlock], bool],
    key: str,
    default=None,
    required: Optional[bool] = None,
    max_steps: int = -1,
    only_active: bool = True,
    docstring: Optional[str] = None,
):
    """Create a function that searches for a value on another block in the hierarchy

    Parameters
    ----------
    subcfg_condition : Callable[[ConfigBlock], bool]
        Condition which the other block must satisfy
    key : str
        The option to read
    default : _type_, optional
        Default value if no matching block is found, by default None
    required : bool, optional
        Whether to raise an exception if no default is found. If left as None it will be
        set to  True if the default value is None.
    max_steps : int, optional
        The number of steps up to allow. A value of 0 means that only the children of
        cfg are searched. A negative value guarantees searching the entire hierarchy.
        Defaults to -1
    only_active : bool, optional
        Only consider active blocks.
    docstring : str, optional
        Docstring of the created function
    """
    if required is None:
        required = default is None

    def f(cfg: ConfigBlock):
        subcfgs = find_blocks(
            cfg, subcfg_condition, max_steps=max_steps, only_active=only_active
        )
        if not subcfgs:
            if required:
                raise ValueError("No matching subconfigs found!")
            else:
                return default
        elif len(subcfgs) > 1:
            raise ValueError(f"{len(subcfgs)} matching subconfigs found (expected 1)")
        return subcfgs[0][key]

    if docstring is None:
        docstring = (
            "Search the hierarchy for a single matching configuration and "
            f"return the key '{key}'"
        )
    f.__doc__ = docstring
    return f


def find_option_on_block_of_type(
    type: Type[ConfigBlock],
    key: str,
    default=None,
    required: Optional[bool] = None,
    max_steps: int = -1,
    only_active: bool = True,
    docstring: Optional[str] = None,
):
    """Create a function that searches for a value on another config block of the specified type in the hierarchy

    Parameters
    ----------
    type : Type[ConfigBlock]
        The type of block to search for
    key : str
        The option to read
    default : _type_, optional
        Default value if no matching block is found, by default None
    required : bool, optional
        Whether to raise an exception if no default is found. If left as None it will be
        set to True if the default value is None.
    max_steps : int, optional
        The number of steps up to allow. A value of 0 means that only the children of
        cfg are searched. A negative value guarantees searching the entire hierarchy.
        Defaults to -1
    only_active : bool, optional
        Only consider active blocks.
    docstring : str, optional
        Docstring of the created function
    """
    if docstring is None:
        docstring = (
            f"Search the hierarchy for a single configuration of type '{type}' "
            f"and return the key '{key}'"
        )
    return find_option_on_matching_block(
        lambda cfg: isinstance(cfg, type),
        key=key,
        default=default,
        required=required,
        max_steps=max_steps,
        only_active=only_active,
        docstring=docstring,
    )
