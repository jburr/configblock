"""Class to print documentation for the options defined by a configuration

This class can be used to print a summary of the options available in a
ConfigBlock hierarchy for a user's documentation.
"""
from __future__ import annotations

import enum
import inspect
import sys
from typing import Optional, TextIO

from configblock.configblock import ConfigBlock, ConfigBlockFactory, OptionData
from configblock.visitorbase import Section, VisitorBase


def get_short_doc(obj) -> str:
    """Get the short docstring from an object"""
    docstr = inspect.getdoc(obj)
    if not docstr:
        return ""
    return docstr.splitlines()[0]


class DocPrinter(VisitorBase):
    """Simple printer"""

    def __init__(
        self,
        stream: TextIO = sys.stdout,
        indent_width: int = 4,
        detail: int = 0,
        only_active: bool = False,
        section_order: tuple[Section, ...] = (
            Section.Options,
            Section.Subconfigs,
            Section.SubconfigTypes,
        ),
    ) -> None:
        """Create the printer

        Parameters
        ----------
        stream : io.TextIOBase
            The output stream, by default sys.stdout
        indent_width : int, optional
            The indent width, by default 4
        detail: int
            Don't print options with a higher detail level than this
        only_active: bool
            Only print from subconfigs that are active
        section_order: Tuple[Section, ...]
            Print information in this order
        """
        super().__init__(only_active=only_active, section_order=section_order)
        self._stream = stream
        self._indent_width = indent_width
        self._detail = detail

    @property
    def detail(self) -> int:
        """Do not print options with a higher detail level than this"""
        return self._detail

    def begin(self) -> bool:
        doc = get_short_doc(self.root)
        if doc:
            self._write(doc)
            self._stream.write("\n")
        return True

    def visit_globals(self) -> None:
        self._write_underlined("Globals")
        assert self.root
        with self.stack_block(self.root.globals):
            for path, data in self.root.globals.iter_option_data(nested=True):
                self.visit_option(str(path), data)

    def begin_globals(self) -> bool:
        self._write_underlined("Globals")
        return True

    def begin_options(self) -> bool:
        n_options = sum(
            self.detail > data.detail
            for _, data in self.current_block.iter_option_data(nested=False)
        )
        if n_options:
            self._write_underlined("Options")
            return True
        else:
            return False

    def visit_option(self, key: str, option: OptionData) -> None:
        if option.detail > self._detail:
            return
        message = f"{key}: {option.help}"
        if not option.required:
            if hasattr(option.default, "description"):
                message += f" [{option.default.description}]"
            elif callable(option.default):
                message += f" [{get_short_doc(option.default)}]"
            else:
                message += f" [{option.default}]"
        if option.choices:
            message += f" ({', '.join(map(self._print_choice, option.choices))})"
        self._write(message)

    def end_options(self) -> None:
        self._stream.write("\n")

    def begin_subcfgs(self) -> bool:
        if self.only_active:
            n_subconfigs = sum(
                cfg.active
                for _, cfg in self.current_block.iter_subconfigs(only_active=True)
            )
        else:
            n_subconfigs = self.current_block.n_subconfigs
        if n_subconfigs:
            self._write_underlined("Subconfigurations")
            return True
        else:
            return False

    def end_subcfgs(self) -> None:
        self._stream.write("\n")

    def begin_subcfg(self, key: str, subcfg: ConfigBlock) -> bool:
        self._write(f"{key}: {get_short_doc(subcfg)}")
        return True

    def visit_default_subcfg_type(
        self, factory: Optional[ConfigBlockFactory], already_output: bool
    ) -> None:
        if factory is None:
            return
        self._write_underlined("Default Subconfiguration Type")
        doc = get_short_doc(factory)
        if doc:
            self._write(doc)
        # Take care of a common case where a block has itself as a default subconfig type
        if not already_output and type(self.current_block) != factory:
            self.visit_block(factory())
        self._stream.write("\n")

    def begin_subcfg_types(self) -> bool:
        if self.current_block.n_extra_subconfig_types:
            self._write_underlined("Subconfiguration Types")
            return True
        else:
            return False

    def end_subcfg_types(self) -> None:
        self._stream.write("\n")

    def visit_subcfg_type(
        self, key: str, factory: ConfigBlockFactory, already_output: bool
    ) -> None:
        self._write(f"{key}: {get_short_doc(factory)}")
        if not already_output:
            self.visit_block(factory())

    def _write(self, text: str) -> None:
        self._stream.write(
            (" " * max(0, self.depth - 1) * self._indent_width) + text + "\n"
        )

    def _write_underlined(self, text: str) -> None:
        self._write(text)
        self._write("-" * len(text))

    def _print_choice(self, choice) -> str:
        # Handle enums correctly
        if isinstance(choice, enum.Enum):
            return choice.name
        else:
            return str(choice)
