"""Base class for visitors to the configblock hierarchy"""

from __future__ import annotations

import contextlib
import enum
from typing import Any, Optional
from configblock.configblock import ConfigBlock, ConfigBlockFactory, OptionData


class Section(enum.Enum):
    """Enum describing the different types of data in each ConfigBlock"""

    #: The options directly defined on this block
    Options = enum.auto()
    #: All subconfigs directly defined on this block, including instantiated extra
    #: subconfigs
    Subconfigs = enum.auto()
    #: The factories on this block, including both the default and any named factories
    SubconfigTypes = enum.auto()


class VisitorBase:
    """Base class for visiting a configblock hierarchy

    Provides a mechanism for iterating through a block in a well defined order.

    Subclasses should provide overrides for all relevant methods. The return
    values of the begin_X methods can be used to control the iteration through
    the hierarchy.
    """

    def __init__(
        self,
        only_active: bool,
        section_order: tuple[Section, ...] = (
            Section.Options,
            Section.Subconfigs,
            Section.SubconfigTypes,
        ),
    ):
        """Create the visitor

        Parameters
        ----------
        only_active : bool
            Only iterate over active subconfigurations
        section_order: Tuple[Section]
            The order in which to iterate. Only sections appearing in this
            tuple will be visited
        """
        self._only_active = only_active
        self._order = section_order
        self._root: Optional[ConfigBlock] = None
        self._stack: list[ConfigBlock] = []
        self._section_methods = {
            Section.Options: self.visit_options,
            Section.Subconfigs: self.visit_subcfgs,
            Section.SubconfigTypes: self.visit_subcfg_types,
        }
        self._factory_stack: list[ConfigBlockFactory] = []

    @property
    def only_active(self) -> bool:
        """Whether this visitor only visits active blocks"""
        return self._only_active

    @property
    def section_order(self) -> tuple[Section, ...]:
        """The section order for this visitor"""
        return self._order

    @property
    def root(self) -> Optional[ConfigBlock]:
        """The root of the hierarchy being visited currently"""
        return self._root

    @property
    def depth(self) -> int:
        """The depth in the hierarchy being visited currently"""
        return len(self._stack)

    @property
    def current_block(self) -> ConfigBlock:
        """The current block in the hierarchy"""
        return self._stack[-1]

    @contextlib.contextmanager
    def stack_block(self, block: ConfigBlock):
        """Add a block to the stack of currently visited blocks"""
        self._stack.append(block)
        try:
            yield
        finally:
            self._stack.pop()

    @contextlib.contextmanager
    def stack_factory(self, factory: Optional[ConfigBlockFactory]):
        """Called when entering a factory to avoid infinite recursion"""
        if factory is None:
            yield False
        else:
            present = factory in self._factory_stack
            self._factory_stack.append(factory)
            try:
                yield present
            finally:
                self._factory_stack.pop()

    def visit_options(self) -> None:
        """Visit the options declared on this block"""
        if self.begin_options():
            for path, data in self.current_block.iter_option_data(nested=False):
                self.visit_option(path.head, data)
            self.end_options()

    def visit_subcfgs(self) -> None:
        """Visit the subconfigurations declared on this block"""
        if self.begin_subcfgs():
            for path, subcfg in self.current_block.iter_subconfigs(
                nested=False, only_active=self.only_active
            ):
                if self.begin_subcfg(path.head, subcfg):
                    self.visit_block(subcfg)
                    self.end_subcfg()
            self.end_subcfgs()

    def visit_subcfg_types(self) -> None:
        """Visit the subconfiguration factories declared on this block"""
        with self.stack_factory(self.current_block.default_subconfig_type) as present:
            self.visit_default_subcfg_type(
                self.current_block.default_subconfig_type, present
            )
        if self.begin_subcfg_types():
            for key, factory in self.current_block.iter_subconfig_types():
                with self.stack_factory(factory) as present:
                    self.visit_subcfg_type(key, factory, present)
            self.end_subcfg_types()

    def visit_block(self, block: ConfigBlock) -> None:
        """Visit a block"""
        with self.stack_block(block):
            if self.begin_block():
                for section in self.section_order:
                    self._section_methods[section]()
                self.end_block()

    def visit_globals(self) -> None:
        """Visit the top global block"""
        if self.begin_globals():
            assert self.root is not None
            self.visit_block(self.root.globals)
            self.end_globals()

    def visit(self, root: ConfigBlock) -> Any:
        """Visit the root block"""
        if not root.is_root:
            raise ValueError("Visit should only be called on the root block")
        self._root = root
        if self.begin():
            self.visit_globals()
            self.visit_block(self._root)
            self.end()
        self._root = None

    def begin(self) -> bool:
        """Begin the output for the provided hierarchy

        Returns
        -------
        bool
            Whether the hierarchy should be entered
        """
        return True

    def end(self) -> None:
        """End the output"""

    def begin_globals(self) -> bool:
        """Begin writing the globals

        Returns
        -------
        bool
            Whether the block should be entered
        """
        return True

    def end_globals(self) -> None:
        """End writing the globals"""

    def begin_block(self) -> bool:
        """Begin writing the specified block

        Returns
        -------
        bool
            Whether the block should be entered
        """
        return True

    def end_block(self) -> None:
        """End writing the current block"""

    def begin_options(self) -> bool:
        """Begin writing the options

        Returns
        -------
        bool
            Whether the options should be iterated over
        """
        return True

    def visit_option(self, key: str, option: OptionData) -> None:
        """Visit a single option"""

    def end_options(self) -> None:
        """End writing the options on the current block"""

    def begin_subcfgs(self) -> bool:
        """Begin writing the subconfigs section

        Returns
        -------
        bool
            Whether the subconfigs section should be iterated over
        """
        return True

    def begin_subcfg(self, key: str, subcfg: ConfigBlock) -> bool:
        """Begin writing a single subconfig

        Note that this method should *not* handle further iteration into the subconfig

        Returns
        -------
        bool
            Whether the subconfig should be entered
        """
        return True

    def end_subcfg(self) -> None:
        """End writing the currrent subconfig"""

    def end_subcfgs(self) -> None:
        """End writing the subconfigs section"""

    def visit_default_subcfg_type(
        self, factory: Optional[ConfigBlockFactory], already_visited: bool
    ) -> None:
        """Visit the default subconfig factory

        Parameters
        ----------
        factory : Optional[ConfigBlockFactory]
            The default subconfig factory for the current block
        already_visited : bool
            Whether or not the information has been output in the current stack. This is
            to prevent infinite recursion where a block has itself as the default type
        """

    def begin_subcfg_types(self) -> bool:
        """Begin writing the subconfig types section

        Returns
        -------
        bool
            Whether to iterate over the subconfiguration factories
        """
        return True

    def visit_subcfg_type(
        self, key: str, factory: ConfigBlockFactory, already_visited: bool
    ) -> None:
        """Visit a single subconfig type

        Parameters
        ----------
        factory : Optional[ConfigBlockFactory]
            The factory to visit
        already_visited : bool
            Whether or not the information has been output in the current stack. This is
            to prevent infinite recursion where a block has itself as the default type
        """

    def end_subcfg_types(self) -> None:
        """End the subconfig types section"""
