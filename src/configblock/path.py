"""Class to represent a path through the configuration"""

from __future__ import annotations

from typing import Any, Iterable, Optional


class Path:
    """Representation of a path through the configuration

    Analysis configurations are formed from nested dictionaries. This class is designed
    to provide access to individual elements within the configuration. An extra
    complication is that individual elements of the configuration are not completely
    independent of each other so in some places we may require a method to access one
    element from another place in the configuration based on some relative path.

    This now looks very similar to a directory structure in an operating system so we
    will use that as a model and use something resembling the (hopefully) familiar POSIX
    path formalism:

    - Nested segments of a path are separated by '/' characters
    - A path beginning with a '/' character is an absolute path beginning from the root
      of the configuration
    - A path not beginning with a '/' character is a relative path beginning from the
      current location in the configuration
    - The current location is represented by a '.' character
    - The location above this one is represented by '..'
    - An empty string translates to '.'
    """

    def __init__(self, path: str | Iterable[str] | Path):
        """Create a path object

        Parameters
        ----------
        path : str, Iterable[str]
            The string representation of the path to build. Can also be a list of the
            path segments

        Raises
        ------
        ValueError
            The path is absolute but contains too many '..' characters (i.e. would
            attempt to advance past the root of the configuration)
        """
        segments: Iterable[str]
        self._segments: tuple[str, ...]
        if isinstance(path, Path):
            self._segments = path._segments
        else:
            if isinstance(path, str):
                # Split the path into segments. By splitting the path into segments like
                # this means that an absolute path is signified by a list of segments
                # beginning with an empty string.
                segments = path.split("/")
                # If the leading piece is "" then replace it by "/" to show that this is
                # an absolute path
                if len(segments) > 1 and segments[0] == "":
                    segments[0] = "/"
                # If there was a trailing '/' character we'll have an extra segment
                # which is just an empty string. If so, remove this
                if segments[-1] == "":
                    segments = segments[:-1]
            else:
                segments = path
            # Remove any empty strings or '.' characters from the core of the path.
            # Don't do this for the first segment as that's used to signify either the
            # root or the current location
            simplified: list[str] = []
            for segment in segments:
                # Remove any internal '' or '.' segments - they add nothing useful
                if segment in (".", ""):
                    continue
                if segment == "..":
                    if len(simplified) > 0 and simplified[-1] != "..":
                        # remove the previous segment
                        previous = simplified.pop()
                        if previous == "/":
                            # If the previous segment was "" then we've reached the top
                            # of an absolute path
                            raise ValueError(f"Invalid path '{path}'")
                        continue  # pragma: no cover
                simplified.append(segment)
            if len(simplified) == 0:
                self._segments = (".",)
            else:
                self._segments = tuple(simplified)

    @property
    def segments(self) -> tuple[str, ...]:
        """The individual segments of the path"""
        return self._segments

    @property
    def head(self) -> str:
        """The first part of the path

        If the path is absolute this is '/'. If it refers to the current location it is
        '.'.
        """
        return self.segments[0]

    @property
    def tail(self) -> Optional[Path]:
        """Path object for everything after the head"""
        return Path(self.segments[1:]) if self.has_tail else None

    @property
    def has_tail(self) -> bool:
        """Whether this path has a tail"""
        return len(self.segments) > 1

    @property
    def absolute(self) -> bool:
        """Whether or not this is an absolute path"""
        return self.head == "/"

    @property
    def refers_to_current_location(self) -> bool:
        """Whether or not this path refers to the current location"""
        return self.head == "."

    @property
    def refers_upward(self) -> bool:
        """Does the head of this path refers to the location above this"""
        return self.head == ".."

    def __str__(self):
        if self.absolute:
            if self.has_tail:
                return f"/{self.tail}"
            else:
                return "/"
        else:
            return "/".join(self.segments)

    def __repr__(self):
        return f'Path("{self}")'

    def __truediv__(self, other: Path | str):
        """Concatenate two paths

        If the rhs of the operator is an absolute path it replaces the existing path
        entirely.

        That is
        >>> Path("a/b/c") / Path("/d/e")
        Path("/d/e")
        but
        >>> Path("a/b/c") / Path("d/e")
        Path("a/b/c/d/e")
        """
        if isinstance(other, str):
            other = Path(other)
        return other if other.absolute else Path(self.segments + other.segments)

    def __rtruediv__(self, other: str):
        """Concatenate two paths"""
        return Path(other) / self

    def __eq__(self, other: Any) -> bool:
        if isinstance(other, str):
            other = Path(other)
        if isinstance(other, Path):
            return self.segments == other.segments
        return NotImplemented

    def __hash__(self) -> int:
        return hash(str(self))

    def __len__(self) -> int:
        """The number of segments in this path"""
        return len(self.segments)

    def __getitem__(self, index: int | slice) -> Path:
        """Return the path formed from the specified segments"""
        return Path(self.segments[index])
