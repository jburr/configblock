"""Class to collect configuration information"""

from __future__ import annotations

import copy
import dataclasses
import enum
import itertools
import logging
from collections.abc import Mapping, MutableMapping
from functools import wraps
import math
from typing import Any, Callable, Iterable, Optional, Type, TypeAlias

from configblock.path import Path

log = logging.getLogger(__name__)


def recurse(func):
    """Wrap a function to recurse through a ConfigBlock

    The provided function must take a ConfigBlock as its first argument and a key
    within that block as its second. The produced wrapper funciton takes care of
    navigating through the hierarchy to the relevant point before calling the wrapped
    function.

    For example, take a very simple function that simply prints the specified value:

    def print_value(cfg: ConfigBlock, key: str) -> None:
        print(cfg[key])

    After recursing, the function effectively behaves as

    def print_value(cfg: ConfigBlock, key: Path) -> None:
        if key.has_tail:
            print_value(cfg[key.head], key.tail)
        else:
            print(cfg[key.head])
    """

    @wraps(func)
    def recurse(self: ConfigBlock, key: str | Path, *args, **kwargs):
        if isinstance(key, str):
            key = Path(key)
        if key.has_tail:
            return recurse(self[key.head], key.tail, *args, **kwargs)
        else:
            return func(self, key.head, *args, **kwargs)

    return recurse


DeferredValue: TypeAlias = Callable[["ConfigBlock"], Any]
"""Function type for set/default valules of options whose evaluation is deferred"""

ConfigBlockFactory: TypeAlias = Callable[[], "ConfigBlock"]
"""Function type to create a new ConfigBlock"""

OptionValidator: TypeAlias = Callable[[Any], bool]
"""Function that throws an exception if the provided value is incorrect"""

__factory_stack: list[ConfigBlockFactory] = []


def _instantiate_factory(factory: ConfigBlockFactory) -> ConfigBlock:
    if factory in __factory_stack:
        return ConfigBlock()
    __factory_stack.append(factory)
    try:
        cfg = factory()
    finally:
        __factory_stack.pop()
    return cfg


@dataclasses.dataclass
class OptionData:
    """Contain all the information about a declared option (before its value is set)"""

    #: The default value
    default: Any | DeferredValue = None
    #: The type of the value - essentially used as a conversion function
    type: Optional[Callable[[Any], Any]] = None
    #: Help string to document the purpose (and potentially a brief summary of the use)
    #: of an option
    help: Optional[str] = None
    #: Whether or not an option is required for the configuration to be valid
    required: bool = False
    #: A list of valid values for the option
    choices: Optional[tuple[Any, ...]] = None
    #: An optional function that should return False if the provided value is incorrect
    #: (also allowed to raise an exception)
    validator: Optional[OptionValidator] = None
    #: How detailed this information is. Classes to print out the full configuration can
    #: use this to decided to skip options
    detail: int = 0


class OptionReference:
    """Helper class to set the value of an option to copy a different option"""

    def __init__(self, path: Path):
        self._path = path

    @property
    def path(self) -> Path:
        """The path this refers to"""
        return self._path

    def __call__(self, config: ConfigBlock):
        """Read the value of the specified option"""
        assert config.is_option(
            self.path
        ), f"Provided path {self.path} is not an option!"
        return config[self.path]

    def __str__(self):
        return f"@{self.path}"

    @property
    def description(self) -> str:
        return str(self)


class ConfigBlock(MutableMapping[str, Any]):
    """Basic unit of the configuration

    Each provides access to a set of configuration options. Blocks can be arranged in a
    hierarchical manner, with each deeper layer configuring more specific and detailed
    information. This allows a deep and complex configuration to be constructed from
    relatively simple blocks.

    Each block can have three different sorts of configuration item in it:

    Options
        Provide a single piece of configuration information. Options are declared by the
        add_option method. Both the set and default values for options can be functions
        which take a single argument (the config block on which they are declared). In
        these cases the evaluation of these options is deferred until they are accessed
        and the function is used to calculate the value at the time of access. This
        allows having options whose values depend on the values of other options in the
        hierarchy

    Predefined subconfigurations
        Nested subconfigurations which are always present (but not necessarily active)
        on this block. These are declared by the add_subconfig function. Usually these
        will only be active if an option on them is set.

    Extra subconfigurations
        Nested subconfigurations which can be provided on the fly through the text
        configuration. The types of these configurations can be set in two ways. Usually
        a 'Type' key in the config file which then maps to a subconfiguration type
        declared by the add_subconfig_type function. Alternatively, a default subconfig
        type can be defined on a block.

    The process of setting up a block goes in two main parts. First the python code sets
    up the set of allowed options, predefined subconfigurations and extra
    subconfiguration types. Then the user provides specific values for the options,
    usually from an external configuration file.

    To reduce boilerplate in the configuration files there are a few shortcuts for
    setting specifc options a block:

    - First, the key it has in its parent object can be used to initialise certain keys
      in the block (note this is only relevant for extra subconfigs).
    - Alternatively, the full block can be set from a single value, usually a string but
      can be any value which is not a Mapping. This is customised through the
      set_from_single_value method.
    """

    @classmethod
    def valid_key(cls, key: str) -> bool:
        """Return whether or not this is a valid key

        Parameters
        ----------
        key : str
            _description_
        """
        return key != "Globals" and not any(x in key for x in (".", "@", "/"))

    def __init__(
        self,
        default_subconfig_type: Optional[ConfigBlockFactory] = None,
    ):
        """Create the block

        Parameters
        ----------
        default_subconfig_type : ConfigBlockFactory, optional
            The factory to use for extra subconfigs if no 'Type' field is specified, by
            default None
        """
        #: The parent of this block
        self.__parent: Optional[ConfigBlock] = None
        #: The key this has in its parent
        self.__key: str = ""
        #: The minimum detail amongst all the options on this block
        self.__detail: int | float = math.inf
        #: Global options (None if this is not the root)
        self._globals: Optional[GlobalConfigBlock] = None
        #: The factory to use for extra subconfigs if no 'Type field is provided
        self._default_subconfig_type: Optional[ConfigBlockFactory] = None
        self.set_default_subconfig_type(default_subconfig_type)
        #: Mapping of type to subconfig factory
        self._subconfig_types: dict[str, ConfigBlockFactory] = {}
        #: The options declared on this object
        self._declared_options: dict[str, OptionData] = {}
        #: Any set values
        self._option_values: dict[str, Any | DeferredValue] = {}
        #: Any predefined subconfigs
        self._predefined_subconfigs: dict[str, ConfigBlock] = {}
        #: Any extra subconfigs which have been added through the configuration
        self._extra_subconfigs: dict[str, ConfigBlock] = {}
        #: The types of the extra subconfigs (for reconstructing the dictionary format)
        self._extra_subconfig_types: dict[str, Optional[str]] = {}
        #: Any options that are set when a non-mapping value is provided
        self._set_from_single_value: set[str] = set()
        #: Whether this subconfig is currently active
        self._active = False
        #: Whether this subconfig is always active (ignores 'clear' calls)
        self._always_active = False

    @property
    def parent(self) -> Optional[ConfigBlock]:
        """The parent block for this block (None if this is the root object)"""
        return self.__parent

    @property
    def key_in_parent(self) -> Optional[str]:
        """The key this has in its parent"""
        return self.__key

    def set_parent(self, parent: ConfigBlock, key: str):
        """Set the parent for this block

        If this currently has a parent it will remove itself from it first

        Parameters
        ----------
        parent : ConfigBlock
            The new parent block
        key : str
            The key in the new parent block
        """
        if self.parent is not None:
            if self.parent is parent and key == self.key_in_parent:
                # Nothing new to be done
                return
            if self.parent.is_extra_subconfig(self.key_in_parent):
                del self.parent[self.key_in_parent]
            elif self.parent.is_predefined_subconfig(self.key_in_parent):
                assert self.key_in_parent
                del self.parent._predefined_subconfigs[self.key_in_parent]
        # Also merge our globals into the new structure
        if parent is not None:
            if not key:
                log.error("A valid key must be set")
                raise ValueError(key)
            if self._globals is not None:
                parent.globals.merge(self._globals)
                self._globals = None
        else:
            # Force the key of the root to be '/'
            key = "/"
        self.__parent = parent
        self.__key = key

    @property
    def is_root(self) -> bool:
        """Whether this is the root block"""
        return self.parent is None

    @property
    def root(self) -> ConfigBlock:
        """Return the root block of the hierarchy"""
        current = self
        while current.parent:
            current = current.parent
        return current

    @property
    def full_path(self) -> Path:
        """The full path for this object"""
        if self.is_root:
            return Path("/")
        else:
            assert self.parent is not None and self.key_in_parent
            return self.parent.full_path / self.key_in_parent

    def activate(self):
        """Make this subconfig active

        Also recurses up the chain and sets all of its parents to be active
        """
        self._active = True
        if self.parent is not None:
            self.parent.activate()

    @property
    def active(self) -> bool:
        """Whether this subconfig is active"""
        return self._active or self._always_active

    @property
    def default_subconfig_type(self) -> Optional[ConfigBlockFactory]:
        """The default subconfig type factory"""
        return self._default_subconfig_type

    def set_default_subconfig_type(self, factory: Optional[ConfigBlockFactory]):
        """Set the default subconfig type factory"""
        self._default_subconfig_type = factory
        if factory is not None:
            self.globals._merge_from_factory(factory)
            self.__detail = min(self.__detail, _instantiate_factory(factory).detail)

    @property
    def globals(self) -> GlobalConfigBlock:
        """The global configs"""
        if self.is_root:
            if self._globals is None:
                self._globals = GlobalConfigBlock()
                self._globals.set_parent(self, "Globals")
            return self._globals
        else:
            return self.root.globals

    @property
    def detail(self) -> int | float:
        """The lowest detail amongst all declared options"""
        return self.__detail

    """---Methods for adding new options/subconfigs---"""

    @recurse
    def add_option(
        self,
        key: str,
        default: DeferredValue | Any = None,
        type: Optional[Callable] = None,
        help: Optional[str] = None,
        required: Optional[bool] = None,
        choices: Optional[Iterable] = None,
        validator: Optional[OptionValidator] = None,
        detail: int = 0,
        set_from_key: bool = False,
        set_from_single_value: bool = False,
        global_default: Optional[str] = None,
    ):
        """Add a new option to this block

        Parameters
        ----------
        key : str or Path
            The name of the option. This forms the key in the configuration file and
            also in this mapping. It must not match any other declared option or any
            block.
        default : DeferredValue or Any, optional
            The default value for this option. This can be a callable object (function)
            which takes the config as an argument.
        type : Callable, optional
            Optional conversion function from the type provided by the configuration to
            the type to be used in the code. Remember that the type from the
            configuration may be inferred by the configuration file parser
        help : str, optional
            Docstring describing the option
        required : bool, optional
            Whether or not this value must be set by the configuration. If left as None
            it will be set to True if the default value is None.
        choices : Iterable, optional
            List of allowed values to be set for this option
        validator : OptionValidator, optional
            Optional function to be called when setting this option. Should return False
            if the set value is invalid. Also allowed to raise an exception in this case
        detail : int, optional
            How detailed this information is. This is used by helper classes which print
            information about the available options. They can choose only to show
            options up to a certain level of detail
        set_from_key : bool, optional
            If this option is not specified it will be set from the key that refers to
            this object in its parent config. Cannot be set alongside default
        set_from_single_value : bool, optional
            If this config is set from a single value rather than a dictionary then this
            option will be set from that value.
        global_default: str
            If set, the default value will instead be set to be a GlobalOption

        Raises
        ------
        ValueError
            The provided key is invalid
        KeyError
            The provided key is already defined in this block
        """
        if not self.valid_key(key):
            raise ValueError(f"Key '{key}' contains invalid characters")
        if key in self:
            raise KeyError(f"Will not redefine key '{key}'")

        if set_from_key:
            if default is not None or global_default:
                raise ValueError("Cannot use set_from_key with a default specified")
            default = lambda cfg: cfg.key_in_parent

        if required is None:
            required = default is None

        # Handle references properly
        if isinstance(default, str):
            if default.startswith("@"):
                default = OptionReference(Path(default[1:]))
            elif default.startswith("\\@"):
                # If the @ is escaped just remove the first character
                default = default[1:]

        data = OptionData(
            default=default,
            type=type,
            help=help,
            required=required,
            choices=tuple(choices) if choices is not None else None,
            validator=validator,
            detail=detail,
        )

        if global_default:
            # Call again but setting the default type properly
            data.detail = 0
            return self.add_option(
                key,
                default=GlobalOption(Path(global_default), data),
                type=type,
                help=help,
                required=required,
                choices=choices,
                validator=validator,
                detail=detail,
                set_from_key=False,
                set_from_single_value=set_from_single_value,
                global_default=None,
            )

        if isinstance(data.default, GlobalOption):
            params = copy.copy(data.__dict__)
            params["default"] = data.default._data.default
            del params["detail"]
            self.add_global_option(
                str(data.default.path).removeprefix("/Globals/"), **params
            )

        self._declared_options[key] = data

        if set_from_single_value:
            self._set_from_single_value.add(key)
        self.__detail = min(self.__detail, detail)

    @recurse
    def add_enum_option(
        self,
        key: str,
        enum_type: Type[enum.Enum],
        default: DeferredValue | Any = None,
        help: Optional[str] = None,
        required: Optional[bool] = None,
        detail: int = 0,
        set_from_key: bool = False,
        set_from_single_value: bool = False,
        global_default: Optional[str] = None,
    ):
        """Add a new option to this block

        Parameters
        ----------
        key : str or Path
            The name of the option. This forms the key in the configuration file and
            also in this mapping. It must not match any other declared option or any
            block.
        enum_type : Type[enum.Enum]
            The type of the enum to be set
        default : DeferredValue or Any, optional
            The default value for this option. This can be a callable object (function)
            which takes the config as an argument.
        help : str, optional
            Docstring describing the option
        required : bool, optional
            Whether or not this value must be set by the configuration. If left as None
            it will be set to True if the default value is None.
        detail : int, optional
            How detailed this information is. This is used by helper classes which print
            information about the available options. They can choose only to show
            options up to a certain level of detail
        set_from_key : bool, optional
            If this option is not specified it will be set from the key that refers to
            this object in its parent config.
        set_from_single_value : bool, optional
            If this config is set from a single value rather than a dictionary then this
            option will be set from that value.
        global_default: str
            If set, the default value will instead be set to be a GlobalOption

        Raises
        ------
        ValueError
            The provided key is invalid
        KeyError
            The provided key is already defined in this block
        """

        def _parse(name_or_value: Any) -> enum.Enum:
            """Parse the either the name or value of an enum and return the enum"""
            if isinstance(name_or_value, enum_type):
                # Already an enum value
                return name_or_value
            elif name_or_value in enum_type._member_names_:
                # This is an enum name
                return enum_type[name_or_value]
            else:
                # Assume this is an enum value
                return enum_type(name_or_value)

        return self.add_option(
            key=key,
            default=default,
            type=_parse,
            help=help,
            required=required,
            choices=tuple(enum_type.__members__.values()),
            detail=detail,
            set_from_key=set_from_key,
            set_from_single_value=set_from_single_value,
            global_default=global_default,
        )

    def add_global_option(
        self,
        key: str | Path,
        default: DeferredValue | Any = None,
        type: Optional[Callable] = None,
        help: Optional[str] = None,
        required: Optional[bool] = None,
        choices: Optional[Iterable] = None,
        validator: Optional[OptionValidator] = None,
    ):
        """Add a global option to this configuration

        Parameters
        ----------
        key : str or Path
            The name of the option. This forms the key in the configuration file and
            also in this mapping. If it matches an existing option then it will be
            merged with it
        default : DeferredValue or Any, optional
            The default value for this option. This can be a callable object (function)
            which takes the config as an argument.
        type : Callable, optional
            Optional conversion function from the type provided by the configuration to
            the type to be used in the code. Remember that the type from the
            configuration may be inferred by the configuration file parser
        help : str, optional
            Docstring describing the option
        required : bool, optional
            Whether or not this value must be set by the configuration. If left as None
            it will be set to True if the default value is None.
        choices : Iterable, optional
            List of allowed values to be set for this option
        validator : OptionValidator, optional
            Optional function to be called when setting this option. Should return False
            if the set value is invalid. Also allowed to raise an exception in this case
        """
        if not isinstance(key, Path):
            key = Path(key)
        cfg = self.globals
        leading = cfg.full_path
        while key.tail is not None:
            if not cfg.is_subconfig(key.head):
                cfg.add_subconfig(key.head, GlobalConfigBlock(), True)
            leading /= key.head
            key = key.tail
        self[leading].add_option(
            key,
            default=default,
            type=type,
            help=help,
            required=required,
            choices=choices,
            validator=validator,
        )

    @recurse
    def add_subconfig(self, key: str, config: ConfigBlock, always: bool = False):
        """Add a new subconfig

        Parameters
        ----------
        key : str or Path
            The name of the subconfiguration. This forms the key in the
            configuration file and also in this mapping. It must not match any
            other declared option or any block.
        config : ConfigBlock
            The configuration block to add
        always : bool
            Whether the block is always active. If False the block will only be
            active if one of its options is set

        Raises
        ------
        ValueError
            The provided key is invalid
        KeyError
            The provided key is already defined in this block
        """
        if not self.valid_key(key):
            raise ValueError(f"Key '{key}' contains invalid characters")
        if key in self:
            raise KeyError(f"Will not redefine key '{key}'")

        self._predefined_subconfigs[key] = config
        config.set_parent(self, key)
        if always:
            config._always_active = True
        self.__detail = min(self.__detail, config.detail)

    def add_subconfig_type(self, key: str, factory: ConfigBlockFactory):
        """Add a new subconfiguration type that can be added as an extra subconfig

        Parameters
        ----------
        key : str
            The name of the subconfig type, will be matched to the 'Type' field in the
            configuration file
        factory : ConfigBlockFactory
            Function that produces a ConfigBlock. Can usually be a ConfigBlock class so
            long as its __init__ method has the correct signature

        Raises
        ------
        KeyError
            The subconfig type name is already registered
        """
        if key in self._subconfig_types:
            raise KeyError(f"Subconfig type '{key}' is already defined!")
        self._subconfig_types[key] = factory
        self.globals._merge_from_factory(factory)
        self.__detail = min(self.__detail, _instantiate_factory(factory).detail)

    """---Methods for removing options/subconfigs---"""

    @recurse
    def remove_option(self, key: str):
        """Remove a named option

        Raises
        ------
        KeyError
            The provided key is not an option
        """
        del self._declared_options[key]
        self._option_values.pop(key, None)
        if key in self._set_from_single_value:
            self._set_from_single_value.remove(key)

    @recurse
    def remove_subconfig(self, name: str):
        """Remove a predefined subconfiguration

        Raises
        ------
        KeyError
            The provided name is not a predefined subconfiguration
        """
        del self._predefined_subconfigs[name]

    def remove_subconfig_type(self, name: str):
        """Remove an already declared subconfig type

        Raises
        ------
        KeyError
            The provided name is not a subconfiguration type
        """
        del self._subconfig_types[name]

    """---Methods for testing the existence/nature of options/subconfigs---"""

    @recurse
    def is_option(self, key: str) -> bool:
        """Test if the specified key is an option"""
        return key in self._declared_options

    @recurse
    def get_option_data(self, key: str) -> OptionData:
        """Get the data for the specified option

        Raises
        ------
        KeyError
            The provided key is not an option
        """
        return self._declared_options[key]

    @recurse
    def is_option_set(self, key: str) -> bool:
        """Whether the specified option is set

        Raises
        ------
        KeyError
            The provided key is not an option
        """
        if not self.is_option(key):
            raise KeyError(key)
        return key in self._option_values

    @recurse
    def is_set_from_single_value(self, key: str) -> bool:
        """Whether the specified option is set from a single value"""
        if not self.is_option(key):
            raise KeyError(key)
        return key in self._set_from_single_value

    @recurse
    def is_predefined_subconfig(self, key: str) -> bool:
        """Returns whether or not the provided key is a predefined subconfig"""
        return key in self._predefined_subconfigs or (self.is_root and key == "/")

    @recurse
    def is_extra_subconfig(self, key: str) -> bool:
        """Returns whether or not the provided key is an extra subconfig"""
        return key in self._extra_subconfigs

    @recurse
    def extra_subconfig_type(self, key: str) -> Optional[str]:
        """The named extra subconfig type

        Raises
        ------
        KeyError
            The key is not an extra subconfig
        """
        return self._extra_subconfig_types[key]

    @recurse
    def is_subconfig(self, key: str) -> bool:
        """Returns whether or not the provided key is a subconfig"""
        return (
            self.is_predefined_subconfig(key)
            or self.is_extra_subconfig(key)
            or (self.is_root and key == "Globals")
        )

    """---Methods for setting and retrieving option values"""

    def set(self, value):
        """Set the values for this config

        If the value is a mapping, this just calls the update function (set key, value
        pairs from the input mapping). Otherwise anything in the
        self._set_from_single_value list is set from that value.
        """
        if not self.active:
            self.activate()
        if isinstance(value, Mapping):
            self.update(value)
        else:
            self.set_from_single_value(value)

    def set_from_single_value(self, value):
        """Set the values for this config from a single (non-dictionary) value"""
        if not self._set_from_single_value:
            log.error(str(self.full_path))
            raise ValueError(
                "Setting from a single value but we have no keys set to receive this"
            )
        for key in self._set_from_single_value:
            self[key] = value

    @recurse
    def get_option_value(self, key: str):
        """Get the value for the specified option

        If the value is not set, the default will be returned instead. If no default is
        set then None will be returned

        Raises
        ------
        KeyError
            The specified key is not an option or a required option is not set
        """
        data = self.get_option_data(key)
        # TODO: This will call the validator every time even if the option was validated
        # when set
        try:
            value = self._option_values[key]
        except KeyError:
            if data.required:
                raise
            value = data.default
            if not callable(value):
                # Do not check the default if it isn't callable
                return value
        return self._check_option_value(data, value)

    @recurse
    def set_option_value(self, key: str, value):
        """Set the value for the specified option

        Can also raise any exception that the validator can raise

        Raises
        ------
        KeyError
            The specified key is not an option
        """
        data = self.get_option_data(key)
        # String values starting with an '@' symbol are references to other options
        if isinstance(value, str):
            if value.startswith("@"):
                value = OptionReference(Path(value[1:]))
            elif value.startswith("\\@"):
                # If the @ is escaped just remove the first character
                value = value[1:]
        if not callable(value):
            value = self._check_option_value(data, value)
        self._option_values[key] = value
        self.activate()

    @recurse
    def remove_option_value(self, key: str):
        """Remove the set value for the specified option

        Raises
        ------
        KeyError
            The specified key is not an option
        """
        if not self.is_option(key):
            raise KeyError(key)
        self._option_values.pop(key, None)

    @recurse
    def get_subconfig(self, key: str) -> ConfigBlock:
        """Get a subconfig (predefined or extra)

        Raises
        ------
        KeyError
            The specified key is not a subconfig
        """
        if key == "/":
            return self.root
        if self.is_predefined_subconfig(key):
            return self._predefined_subconfigs[key]
        elif self.is_extra_subconfig(key):
            return self._extra_subconfigs[key]
        elif self.is_root and key == "Globals":
            return self.globals
        else:
            raise KeyError(key)

    @recurse
    def set_subconfig(self, key: str, value: Any):
        """Set a subconfig

        Raises
        ------
        KeyError
            The specified key already refers to an option or the specified type is
            unknown
        ValueError
            The key is not a predefined subconfiguration, the provided value does not
            contain a 'Type' key and no default subconfig type is set
        """
        if self.is_option(key):
            raise KeyError(f"{key} is an option")
        if self.is_predefined_subconfig(key):
            subconfig = self._predefined_subconfigs[key]
        elif self.is_root and key == "Globals":
            subconfig = self.globals
        else:
            try:
                subconfig = self._extra_subconfigs[key]
            except KeyError:
                # Need to create a new subconfig
                try:
                    subcfg_type: Optional[str] = value.pop("Type")
                except (AttributeError, KeyError):
                    # Not a mapping or the mapping doesn't contain a 'Type' field
                    if self._default_subconfig_type is None:
                        raise ValueError(
                            f"No 'Type' specified for new subconfiguration '{key}' and "
                            "no default subconfig type is set!"
                        ) from None
                    factory = self._default_subconfig_type
                    subcfg_type = None
                else:
                    assert subcfg_type
                    factory = self._subconfig_types[subcfg_type]
                subconfig = factory()
                subconfig.set_parent(self, key)
                subconfig.activate()
                self._extra_subconfigs[key] = subconfig
                self._extra_subconfig_types[key] = subcfg_type
        subconfig.set(value)
        self.activate()

    @recurse
    def reset_predefined_subconfig(self, key: str):
        """Reset all options on the specified subconfig

        Raises
        ------
        KeyError
            The specified key is not a predefined subconfig
        """
        self._predefined_subconfigs[key].clear()

    @recurse
    def remove_extra_subconfig(self, key: str):
        """Completely remove the specified subconfig

        Raises
        ------
        KeyError
            The specified key is not an extra subconfig
        """
        del self._extra_subconfigs[key]
        del self._extra_subconfig_types[key]

    @recurse
    def __getitem__(self, key: str):
        """Retrieve an item from this subconfig

        If retrieving the value of an option that has not been set its default value is
        returned.

        Raises
        ------
        KeyError
            No such path exists
        """
        if key == "/":
            # This is a path referring to the root
            return self.root
        elif key == ".":
            return self
        elif key == "..":
            if self.is_root:
                raise KeyError("..")
            else:
                return self.parent
        elif self.is_option(key):
            return self.get_option_value(key)
        else:
            return self.get_subconfig(key)

    @recurse
    def __setitem__(self, key: str, value: Any):
        """Set an option or a subconfig

        As well as the listed exceptions the individual option validators are allowed to
        raise any exception they please.

        Raises
        ------
        KeyError
            No such path exists
        ValueError
            An invalid value is provided
        """
        if key == "/":
            self.root.set(value)
        elif key == ".":
            self.set(value)
        elif key == "..":
            if self.parent is None:
                raise KeyError("..")
            else:
                self.parent.set(value)
        elif self.is_option(key):
            self.set_option_value(key, value)
        else:
            self.set_subconfig(key, value)

    @recurse
    def __delitem__(self, key: str):
        """Remove a configured option or subconfig

        This behaves differently depending on whether or not the name refers to an
        option, named subconfig or an extra subconfig.

        If the item to be deleted is an option, its stored value is removed (so it
        reverts to its default). If it is a named subconfig then all stored values for
        it are removed (essentially calling delete on every key held in it iteratively)
        and therefore returning it to its default state. If it is an extra subconfig it
        is removed.

        Raises
        ------
        KeyError
            No such path exists
        """
        if key == "/":
            self.root.clear()
        elif key == ".":
            self.clear()
        elif key == "..":
            if self.parent is None:
                raise KeyError("..")
            else:
                self.parent.clear()
        elif self.is_option(key):
            self.remove_option_value(key)
        elif self.is_predefined_subconfig(key):
            self.reset_predefined_subconfig(key)
        elif self.is_extra_subconfig(key):
            self.remove_extra_subconfig(key)
        else:
            raise KeyError(key)

    def clear(self):
        """Remove all configuration-specific information from this.

        All options and configs remain declared, but with their values set back to
        defaults. This is the equivalent of calling del on all keys
        """
        self._option_values.clear()
        for subcfg in self._predefined_subconfigs.values():
            subcfg.clear()
        self._active = False
        self._extra_subconfigs.clear()
        self._extra_subconfig_types.clear()

    def to_dict(self, validate: bool = True) -> dict[str, Any]:
        """Convert this to a basic python dictionary

        Parameters
        ----------
        validate : bool
            If True, first validate the configuration, by default True
        """
        if validate:
            self.validate()
        dct = {str(k): self[k] for k, _ in self.iter_option_data()}
        for key, cfg in self.iter_subconfigs():
            # The configuration has already been validated, no need to do it again
            dct[str(key)] = cfg.to_dict(validate=False)
            if self.is_extra_subconfig(key):
                if (subcfg_type := self._extra_subconfig_types[str(key)]) is not None:
                    dct[str(key)]["Type"] = subcfg_type
        # If this is ROOT add the globals
        if self.is_root:
            globals = self.globals.to_dict()
            if globals:
                dct["Globals"] = globals
        return dct

    def _check_option_value(self, data: OptionData, value) -> Any:
        """Retrieve an option value, making sure it is valid"""
        if callable(value):
            # If the value is deferred then we need to call it
            value = value(self)
        # Pass the value through the type function
        if data.type is not None:
            value = data.type(value)
        # Now check if the value is valid
        if data.choices is not None and value not in data.choices:
            raise ValueError(
                f"Set value '{value}' is not allowed (permitted options are "
                f"{data.choices})"
            )
        if data.validator is not None:
            if not data.validator(value):
                raise ValueError(value)
        return value

    """---Methods for iterating through defined options/subconfigs---"""

    def __len__(self):
        # The length only includes options and active subconfigs to be consistent with
        # __iter__
        return (
            len(self._declared_options)
            + len(self._extra_subconfigs)
            + sum(subcfg.active for subcfg in self._predefined_subconfigs.values())
        )

    def iter_option_data(
        self,
        nested: bool = False,
        only_active: bool = True,
        include_globals=True,
    ) -> Iterable[tuple[Path, OptionData]]:
        """Iterate over the options on this block

        Parameters
        ----------
        nested: bool
            If True, iterate through all nested subconfigs as well.
        only_active: bool
            If True, only look at active nested subconfigs.
        include_globals: bool
            If True, include the global options if nested is also True
        """
        if nested and include_globals and self.is_root:
            for path, data in self.globals.iter_option_data(nested):
                yield "Globals" / path, data
        for opt_key, data in self._declared_options.items():
            yield Path(opt_key), data
        if nested:
            for path, cfg in self.iter_subconfigs(
                nested=nested, only_active=only_active
            ):
                for key, data in cfg.iter_option_data(nested=False):
                    yield path / key, data

    def iter_subconfigs(
        self, nested: bool = False, only_active: bool = True
    ) -> Iterable[tuple[Path, ConfigBlock]]:
        """Iterate over the subconfigs on this block

        Parameters
        ----------
        nested: bool
            If True, iterate through all nested subconfigs as well.
        only_active: bool
            If True, only look at active subconfigs.
        """
        for key, cfg in itertools.chain(
            (
                (k, c)
                for (k, c) in self._predefined_subconfigs.items()
                if not only_active or c.active
            ),
            self._extra_subconfigs.items(),
        ):
            yield Path(key), cfg
            if nested:
                for key2, cfg2 in cfg.iter_subconfigs(
                    nested=nested, only_active=only_active
                ):
                    yield key / key2, cfg2

    def iter_predefined_subconfigs(
        self, nested: bool = False, only_active: bool = True
    ) -> Iterable[tuple[Path, ConfigBlock]]:
        """Iterate over the predefined subconfigs on this block

        Parameters
        ----------
        nested: bool
            If True, iterate through all nested subconfigs as well. This only iterates
            through predefined subconfigs.
        only_active: bool
            If True, only look at active subconfigs.
        """
        for key, cfg in self._predefined_subconfigs.items():
            if not only_active or cfg.active:
                yield Path(key), cfg
            if nested:
                for key2, cfg2 in cfg.iter_predefined_subconfigs(
                    nested=nested, only_active=only_active
                ):
                    yield key / key2, cfg2

    def iter_paths(
        self,
        nested: bool = False,
        only_active: bool = True,
        include_globals=True,
    ) -> Iterable[Path]:
        """Iterate over all paths

        Parameters
        ----------
        nested: bool
            If True, iterate through all nested subconfigs as well.
        only_active: bool
            If True, only look at active subconfigs.
        include_globals: bool
            If True, include the globals
        """
        if include_globals and self.is_root:
            yield Path("Globals")
            if nested:
                for path in self.globals.iter_paths(
                    nested, only_active, include_globals
                ):
                    yield "Globals" / path
        for path, _ in self.iter_option_data(nested=False):
            yield path
        for path, cfg in self.iter_subconfigs(nested=False, only_active=only_active):
            yield path
            if nested:
                for path2 in cfg.iter_paths(nested=nested, only_active=only_active):
                    yield path / path2

    def iter_subconfig_types(self) -> Iterable[tuple[str, ConfigBlockFactory]]:
        """Iterate over the defined subconfig types"""
        return self._subconfig_types.items()

    def __iter__(self):
        return self.iter_paths(nested=False, only_active=True)

    @property
    def n_options(self) -> int:
        """The number of options directly defined on this"""
        return len(self._declared_options)

    @property
    def n_predefined_subconfigs(self) -> int:
        """The number of predefined subconfigs directly defined on this"""
        return len(self._predefined_subconfigs)

    @property
    def n_extra_subconfigs(self) -> int:
        """The number of extra subconfigs directly defined on this"""
        return len(self._extra_subconfigs)

    @property
    def n_subconfigs(self) -> int:
        """The total number of subconfigs defined on this"""
        return self.n_predefined_subconfigs + self.n_extra_subconfigs

    @property
    def n_extra_subconfig_types(self) -> int:
        """The number of extra subconfig types defined on this"""
        return len(self._subconfig_types)

    def _check_errors(self) -> bool:
        """Return true if there is an error in the configuration"""
        error = False
        for key, data in self._declared_options.items():
            try:
                value = self._option_values[key]
            except KeyError:
                # Not set
                if data.required:
                    log.error(f"Required option '{self.full_path / key}' missing!")
                    error = True
                    continue
                value = data.default
            if callable(value):
                # Only check the value if it's a callable. If it isn't then it's either
                # the default which isn't validated or it's a set value which has
                # already been validated.
                try:
                    self._check_option_value(data, value)
                except Exception as e:
                    log.error(
                        f"Setting '{self.full_path / key}' failed with error: {repr(e)}"
                    )
                    error = True
        for _, cfg in self.iter_subconfigs(nested=False, only_active=True):
            error |= cfg._check_errors()
        return error

    def validate(self):
        """Check that the configuration is complete and correct

        All subconfigurations will be checked and a ValueError raised if any are wrong
        """
        if self._check_errors():
            raise ValueError("Invalid configuration!")


class GlobalConfigBlock(ConfigBlock):
    """Config block representing global values

    Global values are special - they are typically used to set default values at several
    places in the configuration. They should not be directly declared, but rather
    declared as default  values for options in the rest of the tree.
    """

    log = log.getChild("GlobalConfigBlock")

    def __init__(self):
        super().__init__()
        self._always_active = True

    def add_subconfig_type(self, key: str, factory: ConfigBlockFactory):
        raise TypeError("Subconfig types are not defined for global blocks")

    @recurse
    def add_option(
        self,
        key: str,
        default: DeferredValue | Any = None,
        type: Optional[Callable] = None,
        help: Optional[str] = None,
        required: Optional[bool] = None,
        choices: Optional[tuple] = None,
        validator: Optional[OptionValidator] = None,
        set_from_key: bool = False,
        set_from_single_value: bool = False,
    ):
        try:
            existing = self.get_option_data(key)
        except KeyError:
            super().add_option(
                key,
                default=default,
                type=type,
                help=help,
                required=required,
                choices=choices,
                validator=validator,
                detail=0,
                set_from_key=set_from_key,
                set_from_single_value=set_from_single_value,
            )
        else:
            if required is None:
                required = default is None
            self._merge_options(
                key,
                existing,
                OptionData(
                    default=default,
                    type=type,
                    help=help,
                    required=required,
                    choices=choices,
                    validator=validator,
                    detail=0,
                ),
            )

    def _merge_options(self, key: str, existing: OptionData, new: OptionData):
        full = self.full_path / key
        default = existing.default
        required = existing.required | new.required
        if existing.default != new.default:
            GlobalConfigBlock.log.warn(
                f"Default values for {full} do not match! There will be no default "
                f"value ('{existing.default}' vs '{new.default}')"
            )
            default = None
            required = True

        help_ = existing.help
        if existing.help != new.help:
            GlobalConfigBlock.log.warn("Help texts for %s do not match!", full)

        choices = None
        if existing.choices is not None:
            if new.choices is None:
                choices = existing.choices
            else:
                choices = tuple(set(existing.choices) & set(new.choices))
                if len(choices) == 0:
                    raise ValueError(f"No valid choice for {full}")
        elif new.choices is not None:
            choices = new.choices

        validator = None
        if existing.validator is not None:
            if new.validator is None:
                validator = existing.validator
            else:
                ex_val = existing.validator
                new_val = new.validator
                validator = lambda val: ex_val(val) and new_val(val)
        elif new.validator is not None:
            validator = new.validator

        self._declared_options[key] = OptionData(
            default=default,
            type=existing.type,
            help=help_,
            required=required,
            choices=choices,
            validator=validator,
            detail=0,
        )

    def merge(self, other: GlobalConfigBlock):
        """Merge this with another config block"""
        # Have to convert the iterator to a tuple as we're going to modify the dict
        for key, data in tuple(other.iter_option_data(nested=False)):
            try:
                existing = self.get_option_data(key)
            except KeyError:
                self._declared_options[key.head] = data
            else:
                self._merge_options(key.head, existing, data)
        for key, cfg in tuple(other.iter_subconfigs(nested=False)):
            try:
                existing = self.get_subconfig(key)
            except KeyError:
                self.add_subconfig(key, cfg, always=cfg._always_active)
            else:
                existing.merge(cfg)

    def _merge_from_factory(self, factory: ConfigBlockFactory):
        """Merge with the globals produced from a factory"""
        self.merge(_instantiate_factory(factory).globals)


class GlobalOption(OptionReference):
    """Helper class to set an option to default to a global option

    Adding an option with this as a default will also add the relevant option into the
    configurations '/Globals' section
    """

    def __init__(self, path: Path, data: OptionData):
        super().__init__("/Globals" / path)
        self._data = data
