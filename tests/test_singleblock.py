from configblock.configblock import ConfigBlock
from operator import itemgetter

cfg = ConfigBlock()
cfg.add_option("Text", default="Hello World!", help="A sample message")
cfg.add_option("Integer", default=10, type=int, help="An integer value")
cfg.add_option("Required", help="A required option")
cfg.add_option(
    "Dynamic",
    default=itemgetter("Text"),
    help="An example whose default copies the value in 'Text'",
)
cfg.add_option("Optional", required=False, help="An optional value")
cfg.add_option(
    "Limited",
    type=int,
    choices=(1, 2, 5),
    help="An integer with a limited number of possible values",
)
cfg.add_option(
    "RangeChecked",
    type=int,
    validator=lambda x: x < 10,
    help="An integer that must be less than 10"
)
cfg.add_option(
    "ValidateDefault",
    default=itemgetter("Integer"),
    validator=lambda x: x > 100,
)