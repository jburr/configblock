========
Overview
========

.. toctree::
    :caption: Contents

    Defining Options <options>
    Adding Subconfigurations <subconfigurations>
    Paths <paths>
    Global Options <globals>

The process of using :python:`ConfigBlocks` usually has three distinct steps.

First all `options <options>`_, `predefined subconfigurations <subconfigurations#predefined-configurations>`_ and `subconfiguration factories <subconfigurations#extra-configurations>`_ are declared.
This will usually be done by whatever code is being configured and then given to the user.
In this documentation this step is called *declaration*.

Next the user sets the values on the declared options.
This step is called *configuration*.
Configuration is done using dictionary semantics.
For example an entire configuration could be loaded from a JSON file:

.. code-block:: python

    with open("config.json") as fp
        cfg.update(json.load(fp))

Lastly the code will process the supplied configuration, which is not handled by this package.

Nothing in the code enforces this separation: you are free to mix the declaring and setting of options as you want but for the purposes of clarity this is not recommended.