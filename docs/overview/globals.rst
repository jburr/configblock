==============
Global Options
==============

Some options may be repeated at multiple points in the hierarchy but still ideally want to share a single value.
Rather than force the user to repeat this value at multiple points in the configuration or set up cumbersome references these values can be set up with a global default during declaration.
This is done by setting the :python:`global_default` parameter in the `add_option <../configblock.configblock.html#configblock.configblock.ConfigBlock.add_option>`_ method to the path to a global option.
This will create an option in the special :python:`"Globals"` block or merge with any existing global option at that path.
The value of the option originally being set will now default to this global option, but can still be set specifically if necessary.

.. code-block:: python

    >>> cfg.add_option("Option1", default="Hello World!", global_default="Default")
    >>> cfg.add_option("Option2", default="Hello World!", global_default="Default")
    >>> cfg["Option1"]
    'Hello World!'
    >>> cfg["/Globals/Default"] = "Goodbye"
    >>> cfg["Option2"]
    'Goodbye'

If the default values for each call which references the global option do not match then the default value for the global option will be removed and it will become required.

.. code-block:: python

    >>> cfg.add_option("Option3", default="Something else", global_default="Default")
    Default values for /Globals/Default do not match! There will be no default value. ('Hello World!' vs 'Something else')
    >>> del cfg["/Globals/Default"]
    >>> cfg.validate()
    Setting '/Option1' failed with error: KeyError('Default')
    Setting '/Option2' failed with error: KeyError('Default')
    Setting '/Option3' failed with error: KeyError('Default')