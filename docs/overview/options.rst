================
Defining Options
================

Overview
========

New options are added to a block using the `add_option <../configblock.configblock.html#configblock.configblock.ConfigBlock.add_option>`_ method.
All options have an optional help string which can then be used to automatically generate documentation for the configuration.

.. code-block:: python

    >>> cfg.add_option("ExampleOption", help="An example option")

Once an option has been added, it can be accessed using normal dictionary access semantics

.. code-block:: python

    >>> cfg["ExampleOption"] = "Hello World!"
    >>> cfg["ExampleOption"]
    "Hello World!"

Attempting to set a key that has not been defined will result in an error.
Deleting the key will not remove the option, but only any value that has been set.
Attempting to access a key whose value has not been set or has been removed in this way results in a :python:`KeyError`.

.. code-block:: python

    >>> del cfg["ExampleOption"]
    >>> cfg["ExampleOption"]
    KeyError: 'ExampleOption'

Instead a default value can be provided for the option which will be returned if no value has been set.

.. code-block:: python

    >>> cfg.add_option("DefaultOption", default="Hello World!", help="An example option with a default")
    >>> cfg["DefaultOption"]
    "Hello World!"

Deleting the value will remove the set value, but not the default.

.. code-block:: python

    >>> cfg["DefaultOption"] = "Goodbye"
    >>> cfg["DefaultOption"]
    "Goodbye"
    >>> del cfg["DefaultOption"]
    >>> cfg["DefaultOption"]
    "Hello World!"

A ``type`` parameter can also be provided which should be a function which will be used to convert any set values (including the default).

.. code-block:: python

    >>> cfg.add_option("IntegerOption", type=int, help="An integer option")
    >>> cfg["IntegerOption"] = "55"
    >>> cfg["IntegerOption"]
    55

Note that the default value will not be passed through the ``type`` function.

Restricting the possible set values
===================================

It is possible to restrict the value that the user is allowed to set.
The simplest way to do this is to provide a list of values to the :python:`choices` parameter.
Attempting to set a value not in this list results in a :python:`ValueError`.

.. code-block:: python

    >>> cfg.add_option("RestrictedOption", type=int, choices=(2, 3, 5), help="An option with a restricted number of values")
    >>> cfg["RestrictedOption"] = 12
    ValueError: Set value '12' is not allowed (permitted options are (2, 3, 5))

Alternatively a custom :python:`validator` function may be added which will be run on any value that is set.
To signal that the set value is invalid the function may either return :python:`False` or raise an exception.

.. code-block:: python

    >>> cfg.add_option("ValidatedOption", type=int, validator=lambda x: x < 10, help="An option with a custom validator")
    >>> cfg["ValidatedOption"] = 12
    ValueError: 12

Where possible setting :python:`choices` should be preferred over a custom :python:`validator` function as more information can be provided to the user.
Default values will not be validated.

Deferred values
===============

Sometimes a value may be determined from other values within the configuration.
In these cases instead of providing a value directly a function can be provided.
Then, whenever that value is accessed the function will be called instead.
The argument to the function will be the block on which the option is declared.

.. code-block:: python

    >>> from operator import itemgetter
    >>> cfg.add_option("Option", default="Default")
    >>> cfg.add_option("Deferred")
    >>> cfg["Deferred"] = itemgetter("Option")
    >>> cfg["Deferred"]
    "Default"
    >>> cfg["Option"] = "SetValue"
    >>> cfg["Deferred"]
    "SetValue"

In this example, an :python:`itemgetter` is provided, which is a function that tries to retrieve the specified item using dictionary-like syntax.
That is, :python:`itemgetter("Option")` is roughly equivalent to :python:`lambda cfg: cfg["Option"]`.
Here, when :python:`cfg["Deferred"]` is called, the block first checks if a value for :python:`"Deferred"` has been provided.
As one has not, the function is called and instead the value set in :python:`"Option"` is returned.

A default value can also be set to a function.
The :python:`type` function and validators (if any) will be applied to the result of the function call, even for the default case (unlike where a simple value is set).

.. code-block:: python

    >>> from operator import itemgetter
    >>> cfg.add_option("Default", default=itemgetter("Other"), type=int, validator=lambda x: x < 20)
    >>> cfg.add_option("Other")
    >>> cfg["Other"] = "5"
    >>> cfg["Default"]
    5
    >>> cfg["Other"] = 100
    >>> cfg["Default"]
    ValueError: 100

Given that referencing another option is a fairly common requirement there is a shortcut for this.
If an option value is a string beginning with an '@' then it will be used to construct an `OptionReference <../configblock.configblock.html#configblock.configblock.OptionReference>`_.
This works both for default and set values.
A normal string beginning with an '@' can be provided by escaping it.

.. code-block:: python

    >>> cfg.add_option("Reference", default="@Other")
    >>> cfg.add_option("Other")
    >>> cfg["Other"] = 10
    >>> cfg["Reference"]
    10
    >>> cfg["Reference"] = "\@Other"
    >>> cfg["Reference"]
    "\@Other"

Required options
================

By default options must be set unless they have a default value different to :python:`None`.
If a default value of :python:`None` should be allowed the the :python:`required` parameter should be set to :python:`False`.

.. code-block:: python

    >>> cfg.add_option("Optional", required=False, help="An optional option")

Enum Options
============

Often an option should be picking from a set of allowed options which are defined by an :python:`enum`.
A shortcut for this is the `add_enum_option <../configblock.configblock.html#configblock.configblock.ConfigBlock.add_enum_option>`_ method.
This sets the :python:`type` and :python:`choices` fields for the option so it accepts any of the string names.

.. code-block:: python

    import enum

    class TestEnum(enum.Enum):
        FirstValue = enum.auto()
        SecondValue = enum.auto()
        ThirdValue = enum.auto()

    cfg.add_enum_option("Option", TestEnum, default=TestEnum.FirstValue, help="An option taking one of the enum values")