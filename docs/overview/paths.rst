=====
Paths
=====

Paths through the configuration can be specified similarly to the POSIX path formalism.

* Nested segments of a path are separated by '/' characters
* A path beginning with a '/' character is an absolute path beginning from the root of the configuration
* A path not beginning with a '/' character is a relative path beginning from the current location in the configuration
* The current location is represented by a '.' character
* The location above this one is represented by '..'
* An empty string translates to '.'

These are represented by the `Path <../configblock.path.html#Path>`_ class.

Many operations such as declaring, setting and accessing options and subconfigurations can be done with a path leading to a different block in the hierarchy.
For example, :python:`cfg["SubCfg1"]["Option"] = "Value"` is exactly equivalent to :python:`cfg["SubCfg1/Option"] = "Value"` and :python:`cfg.parent["SubCfg2"]` is equivalent to :python:`cfg["../SubCfg2"]`.

A function which acts directly on a single :python:`ConfigBlock` can be converted into one which can follow paths through the hierarchy by using the `recurse <../configblock.configblock.html#recurse>`_ decorator.
This takes a function whose first two parameters are a :python:`ConfigBlock` and a :python:`str` which refers to a key in that block and returns a new function which will first navigate to the correct block and then call the wrapped function.