.. configblock documentation master file, created by
   sphinx-quickstart on Fri Aug 12 11:40:52 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. role:: python(code)
    :language: python

=============================
Documentation for configblock
=============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Overview <overview/index>
   Full API <api/index>


Overview
========

``configblock`` is intended to provide a mechanism for creating transparent configurations for
complicated large-scale projects. The created configuration is made out of smaller blocks that can
be composed together into more complex pieces. Each piece in the job can define its own block
needing only minimal information about the rest of the configuration.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
