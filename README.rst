configblock
-----------

Hierarchical, self documenting configuration structure

Please find the full documentation at https://configblock.readthedocs.io/en/latest/
